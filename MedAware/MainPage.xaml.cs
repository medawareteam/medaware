﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.Band;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.Bluetooth.Rfcomm;
using BandAccessFunctions;
using Microsoft.Band.Tiles;
using Windows.Phone.Devices.Notification;
using Windows.Storage;
using Windows.Storage.Search;
using BandConnectionHandler;
using BackgroundTaskFunctions;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MedAware
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static IBandInfo s_pairedBand;

        ContactsViewModel viewModel;

        bool _detectionStarted;



        public MainPage()
        {

            this.InitializeComponent();

            //create database if it doesn't exist
            //SQLitePCL.SQLiteConnection db = new SQLitePCL.SQLiteConnection("medaware.db");
            //Database.LoadDatabase(db);
            //for (int i = 0; i < 5; i++)
            //{
            //    Contact c = new Contact();
            //    c.Name = "Mohammad";
            //    c.PhoneNumber = 9051234567;
            //    c.Priority = 1;
            //    Database.InsertContact(db, c);
            //}

            //Contact c = Database.SelectContact(db, 2);
            //string name = string.Format("Selected Contact: {0}", c.Name);
            //Debug.WriteLine(name);
            CreateTile();




        }

        private static async void CreateTile()
        {
            await BandTileCreator.CreateBandTile();
        }

        private void contactListButton_Click(object sender, RoutedEventArgs e)
        {

            viewModel = ContactsViewModel.GetDefault();
            //TODO: here
            Frame.Navigate(typeof(ContactList)); 
           


        }

        private void settingsButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Settings));
        }

        private void OnStartDetection(object sender, RoutedEventArgs e)
        {
            if (!_detectionStarted)
            {
                BandConnectionHandler.BandConnection.ConnectToBand();
                //_detectionStarted = true;
                try
                {
                    BackgroundTaskProvider.StartFallDetection();
                    //ForegroundFallDetection.StartFallDetection();
                }
                catch
                {
                    BackgroundTaskProvider.UnregisterBandDataTask();
                }
            }
        }

        private async void triggerAction_Click(object sender, RoutedEventArgs e)
        {
            //TODO: need change to toggele button

            //start the flashing visual animation in XAML
            backgroundAnimation.Begin();

            //
            MediaElement soundEffect = new MediaElement();
            Windows.Storage.StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Sound");
            StorageFile file = await folder.GetFileAsync("Alarm10.wav");
            var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
            soundEffect.SetSource(stream, file.ContentType);
            soundEffect.Play();
            soundEffect.IsLooping = true;
        }

    }
}
