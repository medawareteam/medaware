﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedAware
{
    enum ContactType
    {
        Always,
        LocationBased
    }
    public class Contact
    {
        //Properties
        public long Id { get; set; }
        public string Name { get; set; }
        public long PhoneNumber { get; set; }
        //public ContactType ContactType { get; set; }
        public int Priority { get; set; }

    }
}
