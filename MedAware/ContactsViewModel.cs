﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLitePCL;
using System.Diagnostics;

//TODO: delete me
namespace MedAware
{
    class ContactsViewModel : TableViewModelBase<Contact, long>
    {
        private ContactsViewModel() { }
        static ContactsViewModel instance;
        public static ContactsViewModel GetDefault()
        {
            lock (typeof(ContactsViewModel))
            {
                if (instance == null)
                    instance = new ContactsViewModel();
            }
            return instance;
        }

        protected override Contact CreateItem(ISQLiteStatement statement)
        {
            var c = new Contact();
            c.Id = (long)statement[0];
            c.Name = (string)statement[1];
            c.PhoneNumber = (long)statement[2];
            c.Priority = (int)statement[3];

            Debug.WriteLine("Selected Contact name: " + c.Name);

            return c;
        }

        protected override void FillDeleteItemStatement(ISQLiteStatement statement, long key)
        {
            statement.Bind(1, key);
        }

        protected override void FillInsertStatement(ISQLiteStatement statement, Contact item)
        {
            statement.Bind("@name", item.Name);
            statement.Bind("@phonenumber", item.PhoneNumber);
            statement.Bind("@priority", item.Priority);
        }

        protected override void FillSelectAllStatement(ISQLiteStatement statement)
        {
            //nothing to do
        }

        protected override void FillSelectItemStatement(ISQLiteStatement statement, long key)
        {
            statement.Bind(1, key);
            
        }

        protected override void FillUpdateStatement(ISQLiteStatement statement, long key, Contact item)
        {
            statement.Bind(1, item.Name);
            statement.Bind(2, item.PhoneNumber);
            statement.Bind(3, item.Priority);
            statement.Bind(4, key);
        }

        protected override string GetSelectAllSql()
        {
            return "SELECT Id, Name, PhoneNumber, Priority";
        }

        protected override string GetDeleteItemSql()
        {
            return "DELETE FROM Contact WHERE Id = ?";
        }

        protected override string GetInsertItemSql()
        {
            return "INSERT INTO Contact (Name, PhoneNumber, Priority) VALUES (@name, @phonenumber, @priority)";
        }

        protected override string GetSelectItemSql()
        {
            return "SELECT Id, Name, PhoneNumber, Priority FROM Contact WHERE Id = ?";
        }

        protected override string GetUpdateItemSql()
        {
            return "UPDATE Contact SET Name = ?, PhoneNumber = ?, Priority = ? WHERE Id = ?";
        }
    }
}
