﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedAware
{
    class Database
    {
        public static void LoadDatabase(SQLiteConnection db)
        {
            string sql = @"CREATE TABLE IF NOT EXISTS
                                Contact (Id     INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                                            Name VARCHAR(140),
                                            PhoneNumber BIGINT,
                                            Priority INTEGER
                                        );";
            using (var statement = db.Prepare(sql))
            {
                statement.Step();
            }

            //turn on foreign key constraints
            sql = @"PRAGMA foreign_keys = ON";
            using (var statement = db.Prepare(sql))
            {
                statement.Step();
            }
            
        }

        public static void InsertContact(SQLiteConnection db,Contact contact)
        {

            string sql = @"INSERT INTO Contact(Name, PhoneNumber, Priority) VALUES (?, ?, ?)";
            using (var statement = db.Prepare(sql))
            {
                statement.Bind(1, contact.Name);
                statement.Bind(2, contact.PhoneNumber);
                statement.Bind(3, contact.Priority);
                statement.Step();
            }
        }

        public static Contact SelectContact(SQLiteConnection db, long id)
        {
            Contact c = new Contact();
            string sql = @"SELECT Id, Name, PhoneNumber, Priority FROM Contact WHERE Id = ?";
            using (var statement = db.Prepare(sql))
            {
                statement.Bind(1, id);
                statement.Step();               
                string name = (string)statement["Name"];
                long phoneNumber = (long)statement["PhoneNumber"];
                //string wtf = (string)statement[3];
                int priority = (int)((long)statement["Priority"]);
                
                    
                c.Name = name;
                c.PhoneNumber = phoneNumber;
                //c.Priority = priority;
                c.Id = id;
                                     
            }
            
            return c;
        }

        //protected override string GetInsertItemSql()
        //{
        //    return "INSERT INTO Contact (Name, PhoneNumber, Priority) VALUES (@name, @phonenumber, @priority)";
        //}

        //protected override void FillInsertStatement(ISQLiteStatement statement, Contact item)
        //{
        //    statement.Bind("@name", item.Name);
        //    statement.Bind("@phonenumber", item.PhoneNumber);
        //    statement.Bind("@priority", item.Priority);
        //}
    }
}
