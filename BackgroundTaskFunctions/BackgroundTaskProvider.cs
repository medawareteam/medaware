﻿using BandConnectionHandler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.Enumeration;

namespace BandAccessFunctions
{
    public static class BackgroundTaskProvider
    {

        private static async Task<bool> RegisterBandDataTask(string taskName, string entryPoint)
        {
            try
            {
                var access = await BackgroundExecutionManager.RequestAccessAsync();
                await BandConnectionHandler.BandConnection.ConnectToBand();

                if ((access == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity) || (access == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity))
                {

                    var bandTask = new BackgroundTaskBuilder();

                    bandTask.Name = taskName;
                    bandTask.TaskEntryPoint = entryPoint;
                    var trigger = new DeviceUseTrigger();
                    bandTask.SetTrigger(trigger);

                    bandTask.Register();
                    Debug.WriteLine("Task registered");
                    

                    //The magic number GUID should be the one used for all MS bands
                    //Found in the package manifest
                    //Don't know if there is a more readable way to do this.
                    var device = (await DeviceInformation.FindAllAsync(RfcommDeviceService.GetDeviceSelector(RfcommServiceId.FromUuid(new Guid("A502CA9A-2BA5-413C-A4E0-13804E47B38F"))))).FirstOrDefault(x => x.Name == BandConnection.PairedBand.Name);

                    BandConnection.CloseBandConnection();
                    //BandConnection.ConnectToBand();
                    //BandConnection.CloseBandConnection();

                    if (device != null)
                    {
                        var triggerResult = await trigger.RequestAsync(device.Id);

                        switch (triggerResult)
                        {
                            case DeviceTriggerResult.DeniedByUser:
                                throw new InvalidOperationException("Cannot start the background task. Access denied by user.");
                            case DeviceTriggerResult.DeniedBySystem:
                                throw new InvalidOperationException("Cannot start the background task. Access denied by system.");
                            case DeviceTriggerResult.LowBattery:
                                throw new InvalidOperationException("Cannot start the background task. Low battery.");
                        }

                        return true;

                    }
                    else
                    {
                        throw new BandConnectionException("Could not find device information for the band.");
                    }
                }
                else
                {
                    //Task registration failed due to lack of access.
                    return false;
                }
            }
            catch (Exception ex)
            {
                // ToDo 
                throw;
            }
        }

        public static async void StartFallDetection()
        {
            string taskName = "FallDetection";
            string entryPoint = "BackgroundTasks.FallDetection";

            if (TaskRegistered(taskName))
            {
                //todo: handle what happens when task is already registered
            }

            else
            {
                bool result = await RegisterBandDataTask(taskName, entryPoint);
                //todo: do something with the result.
            }
        }

        /// <summary>
        /// Gets the BandDataTask from the BackgroundTaskRegistration provider.
        /// </summary>
        public static IBackgroundTaskRegistration GetRegisteredTask(string taskName)
        {
            var task = BackgroundTaskRegistration.AllTasks.FirstOrDefault(t => t.Value.Name == taskName).Value;
            return task;
        }

        public static bool TaskRegistered(string taskName)
        {
            bool taskRegistered = false;

            //TODO: unregister any background tasks on app exit if required.
            {
                IBackgroundTaskRegistration task = GetRegisteredTask(taskName);
                if ( task != null)
                {
                    taskRegistered = true;
                    Debug.WriteLine("Task already registered.");
                    //Todo: resume task if required
                    //// Debugging code. Remove when not needed ////
                    task.Unregister(true);
                    taskRegistered = false;
                    Debug.WriteLine("Task unregistered");
                    //// TODO: Remove ////
                }
            }
            return taskRegistered;
        }

        public static void UnregisterBandDataTask()
        {
            IBackgroundTaskRegistration task = GetRegisteredTask("FallDetection");
            if (task != null)
            {
                task.Unregister(true);
            }
        }
    }
}
