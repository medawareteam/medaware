﻿using Microsoft.Band;
using Microsoft.Band.Tiles;
using Microsoft.Band.Tiles.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media.Imaging;
using BandConnectionHandler;

namespace BackgroundTaskFunctions
{
    public class BandTileCreator
    {

        //private static readonly int tileCapacity;

        public static async Task CreateBandTile()
        {
            //get the firmware and hardware info of the bandClient.
            //var firmware = await bandClient.GetFirmwareVersionAsync();//show 2.04...
            //var hardware = await bandClient.GetHardwareVersionAsync();//show 26???
            //MessageDialog dialog = new MessageDialog(string.Format("this bandclient firmware: {0}, and the badnclient hardware: {1} ", firmware, hardware));
            //await dialog.ShowAsync();

            //TODO: create the app tile.
            try
            {
                await BandConnectionHandler.BandConnection.ConnectToBand();
                //TODO: get the current set fo tiles, check any tile is used the same name.

                IBandClient bandClient = BandConnection.BandClient;

                IEnumerable<Microsoft.Band.Tiles.BandTile> tiles = await bandClient.TileManager.GetTilesAsync();
                foreach (var t in tiles)
                {
                    // remove the tile from the Band         
                    if (t.Name == "Med-Aware")
                    {
                        // do work if the tile was successfully removed 
                        await bandClient.TileManager.RemoveTileAsync(t);
                    }
                }

                //TODO:check the number of the tile remain avaliable numbers.
                int tileCapacity = await bandClient.TileManager.GetRemainingTileCapacityAsync();
                Debug.WriteLine(tileCapacity.ToString());



                // Create the small and tile icons from writable bitmaps. 
                // Small icons are 24x24 pixels. 
                WriteableBitmap smallIconBitmap = new WriteableBitmap(24, 24);
                BandIcon smallIcon = smallIconBitmap.ToBandIcon();

                // Tile icons are 46x46 pixels for Microsoft Band 1, and 48x48 pixels  
                // for Microsoft Band 2. 
                WriteableBitmap tileIconBitmap = new WriteableBitmap(48, 48);
                BandIcon tileIcon = tileIconBitmap.ToBandIcon();

                // create a new Guid for the tile 
                Guid tileGuid = Guid.NewGuid();
                // create a new tile with a new Guid 
                BandTile tile = new BandTile(tileGuid)
                {
                    // enable badging (the count of unread messages)     
                    IsBadgingEnabled = true,
                    // set the name    
                    Name = "Med-Aware",
                    // set the icons
                    SmallIcon = await LoadIcon("ms-appx:///Assets/SmallTile_24x24.png"),
                    TileIcon = await LoadIcon("ms-appx:///Assets/LargeTile_48x48.png"),
                };
                // add the tile to the Band     
                if (await bandClient.TileManager.AddTileAsync(tile))
                {
                    //TODO: if the tile is added successfully, then...
                    Debug.WriteLine("Just created the tile");
                }

            }
            catch (BandException ex)
            { 
                MessageDialog dialog1 = new MessageDialog("Band bluetooth, paired or create tile problem", ex.ToString());
                await dialog1.ShowAsync();
            }
            finally
            {
                BandConnectionHandler.BandConnection.CloseBandConnection();
            }
        }

        private static async Task<BandIcon> LoadIcon(string uri)
        {
            var imageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri(uri));

            using (var fileStream = await imageFile.OpenAsync(FileAccessMode.Read))
            {
                var bitmap = new WriteableBitmap(1, 1);
                await bitmap.SetSourceAsync(fileStream);
                return bitmap.ToBandIcon();
            }
        }
    }
}
