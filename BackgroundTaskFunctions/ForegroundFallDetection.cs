﻿using Microsoft.Band;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Band.Sensors;
using System.Diagnostics;
using BandConnectionHandler;

namespace BandAccessFunctions
{
    public static class ForegroundFallDetection
    {
        const double ACCELERATIONLIMIT = 6;
        private static IBandClient _bandClient;

        private static async Task AccessBandSensors()
        {
            if (_bandClient != null)
            {
                if (_bandClient.SensorManager.Accelerometer.GetCurrentUserConsent() != UserConsent.Granted)
                {
                    //No access to accelerometer, complete the deferral
                    // ToDo: tell user the app needs sensor access to function?
                    Debug.WriteLine("Accelerometer access denied");
                    return;
                }
                else
                {
                    _bandClient.SensorManager.Accelerometer.ReportingInterval = TimeSpan.FromMilliseconds(32.0); //16, 32 or 128
                    _bandClient.SensorManager.Accelerometer.ReadingChanged += OnAccelerationChanged;
                    Debug.WriteLine("Accelerometer accessed, beginning fall detection.");
                    await _bandClient.SensorManager.Accelerometer.StartReadingsAsync();
                }
            }
        }

        private static void OnAccelerationChanged(object sender, BandSensorReadingEventArgs<IBandAccelerometerReading> e)
        {
            //var bandContactState = await _bandClient.SensorManager.Contact.GetCurrentStateAsync();

            //if (bandContactState.State == BandContactState.NotWorn)
            //{
                //TODO: handle what happens when the user isn't wearing the band.
            //}

            if (FallDetected(e))
            {
                Debug.WriteLine("Fall Detected!");
                //TODO: Handle what happens when a fall is detected.
            }
        }

        private static bool FallDetected(BandSensorReadingEventArgs<IBandAccelerometerReading> accelReading)
        {
            double xAccel = accelReading.SensorReading.AccelerationX;
            double yAccel = accelReading.SensorReading.AccelerationY;
            double zAccel = accelReading.SensorReading.AccelerationZ;

            double magnitude = Math.Sqrt(Math.Pow(xAccel, 2) + Math.Pow(yAccel, 2) + Math.Pow(zAccel, 2));
            if (magnitude > ACCELERATIONLIMIT)
            {
                //Fall was detected
                return true;
            }
            else
            {
                return false;
            }
        }

        public static async void StartFallDetection()
        {
            _bandClient = await BandClientManager.Instance.ConnectAsync(BandConnection.PairedBand);
            if (_bandClient != null)
            {
                await AccessBandSensors();
            }
        }

    }
}
