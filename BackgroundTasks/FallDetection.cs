﻿using Microsoft.Band;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Microsoft.Band.Sensors;
//using BandConnectionHandler;

namespace BackgroundTasks
{
    public sealed class FallDetection : IBackgroundTask
    {

        const double ACCELERATIONLIMIT = 6;
        BackgroundTaskDeferral _deferral;
        private IBandClient _bandClient;
        private bool _fallDetected;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            
            _deferral = taskInstance.GetDeferral();
            taskInstance.Canceled += this.OnTaskCanceled;

            try
            {
                Debug.WriteLine("Fall detection background task created.");
                await AccessBandSensors();
            }
            catch (Exception)
            {
                await CompleteDeferral();
                throw;
            }
            
        }

        

        private async Task AccessBandSensors()
        {
            //Use a separate project for connecting to the band?
            //BandConnection.ConnectToBand();
            var bandInfo = (await BandClientManager.Instance.GetBandsAsync()).FirstOrDefault();
            try
            {
                _bandClient = await BandClientManager.Instance.ConnectAsync(bandInfo);
            }
            catch (Exception)
            {
                //Problem with connecting
                throw;
            }
            if (_bandClient != null)
            {
                if (this._bandClient.SensorManager.Accelerometer.GetCurrentUserConsent() != UserConsent.Granted)
                {
                    //No access to accelerometer, complete the deferral
                    // ToDo: tell user the app needs sensor access to function?
                    await CompleteDeferral();
                    return;
                }
                else
                {
                    _bandClient.SensorManager.Accelerometer.ReportingInterval = TimeSpan.FromMilliseconds(32.0); //16, 32 or 128
                    _bandClient.SensorManager.Accelerometer.ReadingChanged += OnAccelerationChanged;
                    Debug.WriteLine("Accelerometer accessed, beginning fall detection.");
                    await _bandClient.SensorManager.Accelerometer.StartReadingsAsync();
                }
            }
        }

        private async Task<bool> FallDetected(BandSensorReadingEventArgs<IBandAccelerometerReading> accelReading)
        {
            double xAccel = accelReading.SensorReading.AccelerationX;
            double yAccel = accelReading.SensorReading.AccelerationY;
            double zAccel = accelReading.SensorReading.AccelerationZ;

            double magnitude = Math.Sqrt(Math.Pow(xAccel, 2) + Math.Pow(yAccel, 2) + Math.Pow(zAccel, 2));
            if (magnitude > ACCELERATIONLIMIT && _fallDetected != true)
            {
                //Fall was detected for the first time.
                _fallDetected = true;
                var bandContactState = await _bandClient.SensorManager.Contact.GetCurrentStateAsync();
                if (bandContactState.State == BandContactState.Worn)
                {
                    //Band is currently worn.
                    return true;
                }
                else
                {
                    //Band is not being worn.
                    //TODO: handle what happens when the band is not worn.
                    Debug.WriteLine("Fall detected but user was not wearing the band.");
                    return true; //todo remove for my unwearabe band
                    _fallDetected = false;
                }
            }
            
            return false;
        }

        //Make async?
        private async void OnAccelerationChanged(object sender, BandSensorReadingEventArgs<IBandAccelerometerReading> e)
        {
            //Debug.WriteLine("Acceleration changed");
            //Checking contact state each time will clog up everything and create infinite threads. Only do it once.
            //Maybe when a fall is detected, and/or when app is started.
            //Could add event handler to only read accelerometer when band is worn.
            //var bandContactState = await this._bandClient.SensorManager.Contact.GetCurrentStateAsync();

            

            if (await FallDetected(e))
            {
                Debug.WriteLine("Fall Detected!");
                //TODO: Handle what happens when a fall is detected.
                await CompleteDeferral();
            }
        }

        private async void OnTaskCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            await CompleteDeferral();
        }

        private async Task CompleteDeferral()
        {
            Debug.WriteLine("Completing deferral...");
            if (this._bandClient != null)
            {
                await _bandClient.SensorManager.Accelerometer.StopReadingsAsync();
                this._bandClient.Dispose();//Should be done in BandConnection?
                this._bandClient = null;
            }
            //BackgroundTaskProvider.UnregisterBandDataTask();

            this._deferral.Complete();//finish the task and need register again.
        }
    }
}
