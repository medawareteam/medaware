﻿using Microsoft.Band;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Microsoft.Band.Tiles;
using Microsoft.Band.Tiles.Pages;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage;

namespace BandConnectionHandler
{

    public class BandConnectionException : Exception
    {
        public BandConnectionException(string message) : base(message) { }
    }

    public static class BandConnection
    {

        static IBandInfo s_pairedBand;
        private static readonly string tileCapacity;

        static IBandClient s_bandClient;

        public static IBandInfo PairedBand
        {
            get {
                if (s_pairedBand == null)
                {
                    //throw new BandConnectionException("Tried to access paired band before it has been defined. Call ConnectToBand first.");
                    //ConnectToBand();
                }
                return s_pairedBand;
            }
        }

        //Connect every time?
        public static IBandClient BandClient
        {
            get
            {
                if (s_bandClient == null)
                {
                    //ConnectToBand();
                }
                return s_bandClient;
            }
        }

        private static async Task FindPairedBand()
        {
            //Todo: handle multiple paired bands
            //Debug.WriteLine(BandClientManager.Instance.GetBandsAsync());
            s_pairedBand = (await BandClientManager.Instance.GetBandsAsync()).FirstOrDefault();
            if (s_pairedBand == null)
            {
                throw new BandConnectionException("No paired band found.");
            }
            
        }


        //TODO: handle disposal properly
        private static async Task CheckBandStatus()
        {
            s_bandClient = null;
            bool isRunning = false;

            if (s_pairedBand != null)
            {
                try
                {
                    s_bandClient = await BandClientManager.Instance.ConnectAsync(s_pairedBand);
                    
                }
                catch (Exception ex)
                {
                    //todo
                    // Problem during connection
                    throw;
                }

                if (s_bandClient != null)
                {


                    if (s_bandClient.SensorManager.Accelerometer.GetCurrentUserConsent() != UserConsent.Granted)
                    {
                        await s_bandClient.SensorManager.Accelerometer.RequestUserConsentAsync();
                    }
                    if (s_bandClient.SensorManager.Accelerometer.GetCurrentUserConsent() == UserConsent.Granted)
                    {
                        Debug.WriteLine("Band detected and the user has granted access");
                    }
                }
            }
        }

        /// <summary>
        /// Finds a paired band and connects to it.
        /// When you call this you MUST call CloseBandConnection once you are done with the connection.
        /// </summary>
        public static async Task ConnectToBand()
        {
            await FindPairedBand();
            await CheckBandStatus();
             
        }

        /// <summary>
        /// Closes the connection to the band and resets related variables.
        /// Always call this after calling ConnectToBand
        /// </summary>
        public static void CloseBandConnection()
        {
            if (s_bandClient != null)
            {
                s_bandClient.Dispose();
                s_bandClient = null;
                s_pairedBand = null;
            }
        }

    }
}
